# 让GIT BASH支持make

git的bash实际上也就是一个mingw，是可以支持部分linux指令的，但是只有少部分。在编译代码的时候经常会使用make命令反而在bash下默认是不支持的。

当然是有办法可以解决的：
- 到 https://sourceforge.net/projects/ezwinports/files/ 去下载 make-4.4.1-without-guile-w32-bin.zip
- 把该文件进行解压
- 把解压出来的文件全部拷贝的git的安装目录下： .\Program Files\Git\mingw64\,把文件夹进行合并，如果跳出来需要替换的文件要选择不替换
- 这样在git bash窗口下就可以执行make了（编译的时候编译器还是需要另外安装的）
