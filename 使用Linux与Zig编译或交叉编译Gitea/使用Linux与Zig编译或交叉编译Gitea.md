# 使用Linux与Zig编译或交叉编译Gitea
- 下载：https://ziglang.org/download/
- 安装：
```sh
sudo rm -rf /usr/local/zig && tar -C /usr/local -xf zig-linux-x86_64-0.10.1.tar.xz

export PATH=$PATH:/usr/local/zig-linux-x86_64-0.10.1/
```

编译：

### ️1️⃣编译(Linux ➝ Linux)

```sh
CC="zig cc -target x86_64-linux-gnu" \
CGO_ENABLED=1 \
CGO_CFLAGS="-O2 -g -pthread" \
CGO_LDFLAGS="-linkmode=external -v"
GOOS=linux \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```
✅亲测成功
```sh
CC="zig cc -target x86_64-linux-gnu -isystem /usr/include -L/usr/lib/x86_64-linux-gnu" \
.
.
.
```
✅亲测成功

```sh
CC="zig cc -target x86_64-linux-gnu.2.31" \
.
.
.
```
✅亲测成功

### 2️⃣交叉编译(Linux ➝ Windows)
```sh
CC="zig cc -target x86_64-windows-gnu" \
CGO_ENABLED=1 \
CGO_CFLAGS="-O2 -g -pthread" \
GOOS=windows \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```
✅亲测成功(Gitea版本1.20.0-rc0,编译后96.4M，选择SQLite3可以正常安装）