# README

```
CC="zig cc -target x86_64-linux-gnu -isystem /usr/include -L/usr/lib/x86_64-linux-gnu" \
CGO_ENABLED=1 \
GOOS=linux \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```
出错：

```
# code.gitea.io/gitea
/usr/local/go/pkg/tool/linux_amd64/link: running zig failed: exit status 1
ld.lld: error: undefined symbol: res_search
>>> referenced by cgo-gcc-prolog:60
>>>               /tmp/go-link-3318397556/000005.o:(_cgo_cbcce81e6342_C2func_res_search)
>>> referenced by cgo-gcc-prolog:86
>>>               /tmp/go-link-3318397556/000005.o:(_cgo_cbcce81e6342_Cfunc_res_search)
>>> did you mean: re_search
>>> defined in: /home/mx/.cache/zig/o/4647bd6623097bd284f1f08b0e2501cd/libc.so.6

ld.lld: error: undefined symbol: fcntl64
>>> referenced by sqlite3-binding.c
>>>               /tmp/go-link-3318397556/000017.o:(aSyscall)

make: *** [Makefile:790：gitea] 错误 1

```

https://github.com/golang/go/issues/52690#issuecomment-1116718051
尝试增加：
CGO_LDFLAGS="-linkmode=external -v"

```
CC="zig cc -target x86_64-linux-gnu -isystem /usr/include -L/usr/lib/x86_64-linux-gnu" \
CGO_ENABLED=1 \
CGO_CFLAGS="-O2 -g -pthread" \
CGO_LDFLAGS="-linkmode=external -v"
GOOS=linux \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```

成功

```
CC="zig cc -target x86_64-linux-gnu" \
CGO_ENABLED=1 \
CGO_CFLAGS="-O2 -g -pthread" \
CGO_LDFLAGS="-linkmode=external -v"
GOOS=linux \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```