# README
1.成功编译
```sh
CC="zig cc -target x86_64-windows-gnu" \
CGO_ENABLED=1 \
GOOS=windows \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```
大小：96.2 MiB (100,913,152 字节)

2.增加CGO_CFLAGS成功编译
```
CC="zig cc -target x86_64-windows-gnu" \
CGO_ENABLED=1 \
CGO_CFLAGS="-O2 -g -pthread" \
GOOS=windows \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```