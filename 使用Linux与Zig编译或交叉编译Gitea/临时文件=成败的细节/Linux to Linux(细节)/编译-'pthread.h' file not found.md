# 'pthread.h' file not found

```
# runtime/cgo
gcc_libinit.c:8:10: fatal error: 'pthread.h' file not found
#include <pthread.h>
         ^~~~~~~~~~~
1 error generated.

```

原来由于pthread库不是Linux系统默认的库，连接时需要使用库libpthread.a,所以在使用pthread_create创建线程时，在编译中要加-lpthread参数，或者 -pthread

gcc -o main -pthread main.c


```
# runtime/cgo
gcc_libinit.c:8:10: fatal error: 'pthread.h' file not found
#include <pthread.h>
         ^~~~~~~~~~~
1 error generated.

```
Fix:
增加：CGO_CFLAGS="-O2 -g -pthread"

```diff
CC="zig cc -target x86_64-linux-gnu" \
CGO_ENABLED=1 \
CGO_CFLAGS="-O2 -g -pthread" \
GOOS=linux \
GOARCH=amd64 \
TAGS="bindata sqlite sqlite_unlock_notify" \
make build
```